import cv2
import os
cam = cv2.VideoCapture(0)
cam.set(3, 640) # ширина окна
cam.set(4, 480) # длина окна
face_detector = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
while(True):
    ret, img = cam.read()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) #градации серого
    faces = face_detector.detectMultiScale(gray, 1.3, 5)
    for (x,y,w,h) in faces:
        cv2.rectangle(img, (x,y), (x+w,y+h), (255,0,0), 2)     
        cv2.imshow('image', img)
    k = cv2.waitKey(100) & 0xff # Нажать 'ESC', чтобы выйти
    if k == 27:
        break
cam.release()
cv2.destroyAllWindows()